# Custom Rule Groups for Little Snitch

[lsrg]: https://help.obdev.at/littlesnitch5/lsc-rule-group-subscriptions

Click on any of these links to add them to your [Little Snitch rule groups][lsrg].

* [1Password](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fgitlab.com%2FJohnTrammell%2Flittle-snitch%2F-%2Fraw%2Fmain%2F1Password.lsrules)
* [Dropbox](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fgitlab.com%2FJohnTrammell%2Flittle-snitch%2F-%2Fraw%2Fmain%2FDropbox.lsrules)
* [Google Services](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fgitlab.com%2FJohnTrammell%2Flittle-snitch%2F-%2Fraw%2Fmain%2FGoogle%2520Services.lsrules)
* [Mendeley](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fgitlab.com%2FJohnTrammell%2Flittle-snitch%2F-%2Fraw%2Fmain%2FMendeley.lsrules)
* [Slack Services](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fgitlab.com%2FJohnTrammell%2Flittle-snitch%2F-%2Fraw%2Fmain%2FSlack%2520Services.lsrules)

> Note: to get Little Snitch URLs with spaces to work correctly, you need to
> *double-encode* the spaces in the URL. So for example, in the `Google Services`
> URL above, instead of `Google Services` or `Google%20Services`, use the string
> `Google%2520Services`.


## References

* https://support.1password.com/ports-domains/

